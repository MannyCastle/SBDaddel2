package com.sbdaddel2.sbdaddel2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbDaddel2Application {

    public static void main(String[] args) {
        SpringApplication.run(SbDaddel2Application.class, args);
    }

}
