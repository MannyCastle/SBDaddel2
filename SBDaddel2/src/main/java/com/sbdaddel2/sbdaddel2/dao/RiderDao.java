package com.sbdaddel2.sbdaddel2.dao;

import com.sbdaddel2.sbdaddel2.dto.RiderDto;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RiderDao {

    private List<RiderDto> riderList = List.of(
            RiderDto.builder().riderId("A01").name("Johanna").yearsExperience(2).build(),
            RiderDto.builder().riderId("A02").name("Richie").yearsExperience(1).build()
    );

    public List<RiderDto> getRiders() {
        return riderList;
    }
}
