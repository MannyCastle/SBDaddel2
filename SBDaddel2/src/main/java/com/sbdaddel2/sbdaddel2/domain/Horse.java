package com.sbdaddel2.sbdaddel2.domain;

import lombok.Builder;

@Builder
public class Horse {
    String name;
    String race; //enum better here ?

    Double cost;

}
