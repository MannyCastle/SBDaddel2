package com.sbdaddel2.sbdaddel2.domain;

import lombok.Data;

@Data
public class Corporation {
    public String name;
    public Integer numberOfEmployees;
    public Double yearlyIncome;

    public Corporation(String name, Integer numberOfEmployees, Double yearlyIncome) {
        this.name = name;
        this.numberOfEmployees = numberOfEmployees;
        this.yearlyIncome = yearlyIncome;
    }
}
