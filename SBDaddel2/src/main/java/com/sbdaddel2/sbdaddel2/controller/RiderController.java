package com.sbdaddel2.sbdaddel2.controller;

import com.sbdaddel2.sbdaddel2.dao.RiderDao;
import com.sbdaddel2.sbdaddel2.dto.RiderDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RiderController {

    private final RiderDao riderDao;

    public RiderController(RiderDao riderDao) {
        this.riderDao = riderDao;
    }

    @GetMapping("/riders")
    public List<RiderDto> riders(){
        return riderDao.getRiders();
    }
}
