package com.sbdaddel2.sbdaddel2.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class HorseDto {

    private String name;
    private Double age;
    private String description;
    private Boolean hasBeenOperated;

    enum HorseRaces {
        HANNOVERANIAN,
        IRISH,
        FRISIAN,
        ARAB,
        ANDALUSIAN,
        LYPISIAN,
        HOLSTEEN
    }


}
