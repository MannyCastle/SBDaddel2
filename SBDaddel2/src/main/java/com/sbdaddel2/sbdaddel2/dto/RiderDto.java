package com.sbdaddel2.sbdaddel2.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class RiderDto {

    private String riderId;
    private String name;
    private Integer yearsExperience;

}
